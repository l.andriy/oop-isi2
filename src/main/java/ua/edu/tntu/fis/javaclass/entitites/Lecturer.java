package ua.edu.tntu.fis.javaclass.entitites;

import java.util.Arrays;

import ua.edu.tntu.fis.javaclass.entitites.organs.Brain;
import ua.edu.tntu.fis.javaclass.entitites.organs.Heart;

public class Lecturer extends Human {
	
	private Education education;
	
	public Lecturer() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Lecturer(int id, double weight, double height, int age, Brain brain, Heart heart, Stomach stomach,
			Bug[] bugs, String name, Gender sex) {
		super(id, weight, height, age, brain, heart, stomach, bugs, name, sex);
		// TODO Auto-generated constructor stub
	}



	public Lecturer(int id, double weight, double height, int age, String name, Bug[] bugs, Gender sex) {
		super(id, weight, height, age, name, bugs, sex);
		// TODO Auto-generated constructor stub
	}



	public Lecturer(int id, double weight, double height, int age, String name, Gender sex, Education education) {
		super(id, weight, height, age, name, sex);
		// TODO Auto-generated constructor stub
		this.education = education;
	}



	public Lecturer(int id, double weight, double height, int age, Brain brain, Heart heart, Stomach stomach,
			Bug[] bugs, String name, Gender sex, Education education) {
		super(id, weight, height, age, brain, heart, stomach, bugs, name, sex);
		// TODO Auto-generated constructor stub
		this.education = education;
	}



	public void teach(Animal student) {
		System.out.println("I'm teaching " + student.getName());
	}



	@Override
	public String toString() {
		return "Lecturer [education=" + education + ", create()=" + create() + ", getId()=" + getId() + ", getWeight()="
				+ getWeight() + ", getHeight()=" + getHeight() + ", getAge()=" + getAge() + ", getBugs()="
				+ Arrays.toString(getBugs()) + ", getName()=" + getName() + ", getSex()=" + getSex() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
	

}

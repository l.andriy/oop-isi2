package ua.edu.tntu.fis.javaclass.entitites;

import ua.edu.tntu.fis.javaclass.entitites.organs.Brain;
import ua.edu.tntu.fis.javaclass.entitites.organs.Heart;

public class Dog extends Animal implements Swimmable {
	
	

	public Dog() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Dog(int id, double weight, int age, Bug[] bugs,
			String name, Gender sex) {
		super(id, weight, age, bugs, name, sex);
		// TODO Auto-generated constructor stub
	}

	public Dog(int id, double weight, double height, int age, Brain brain, Heart heart, Stomach stomach, Bug[] bugs,
			String name, Gender sex) {
		super(id, weight, height, age, brain, heart, stomach, bugs, name, sex);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void talk() {
		System.out.println("I'm a dog, I'm barking");
	}

	@Override
	public void love(Animal animal) {
		if (animal instanceof Dog) {
			System.out.println(this.getName() + " loves " + animal.getName());
		} else {
			System.out.println("it is impossible to have love");
		}
		
	}

}

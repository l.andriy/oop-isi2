package ua.edu.tntu.fis.javaclass.runner;

import java.util.Arrays;

import ua.edu.tntu.fis.javaclass.entitites.Animal;
import ua.edu.tntu.fis.javaclass.entitites.Bug;
import ua.edu.tntu.fis.javaclass.entitites.Dog;
import ua.edu.tntu.fis.javaclass.entitites.Education;
import ua.edu.tntu.fis.javaclass.entitites.Gender;
import ua.edu.tntu.fis.javaclass.entitites.God;
import ua.edu.tntu.fis.javaclass.entitites.Lecturer;
import ua.edu.tntu.fis.javaclass.entitites.Student;

public class App {

	public static void main(String[] args) {
		Bug[] bugs = new Bug[] {new Bug("Buggy1"), new Bug("Buggy1"), new Bug("Buggy3")};
		System.out.println("Bugs: " + Arrays.toString(bugs));
		God student1 = new Student(1, 70.0, 180, 20, "Akim", Gender.MALE, 95);
		System.out.println(student1);
		Animal student2 = new Student(2, 79.0, 170, 21, "Igor", Gender.MALE, 93);
		System.out.println(student2);
		Animal student3 = new Student(2, 79.0, 170, 21, "Zenyk", Gender.MALE, 93);
		System.out.println(student3);
		Animal dog1 = new Dog(3, 10, 20, bugs, "Rex", Gender.MALE);
		System.out.println(dog1);
		Dog dog2 = new Dog(4, 7, 10, bugs, "Layka", Gender.FEMALE);
		Dog dog3 = new Dog(4, 7, 10, bugs, "Layka3", Gender.FEMALE);
		System.out.println(dog2);
		
		for (int i = 0; i < bugs.length; i++) {
			System.out.println(bugs[i]);
			bugs[i].bite(dog2);
		}
		
		dog2.talk();
		dog2.swim(10, 50);
		dog3.swim(40,  20);
//		dog1.swim(10, 50); due to Animal which is not swimmable
		
		
		God lecture1 = new Lecturer(1, 70.0, 180, 30, "Bogdan", Gender.MALE, Education.PHD);
		System.out.println(lecture1);
		Animal lecture2 = new Lecturer(2, 79.0, 170, 51, "Serhiy", Gender.MALE, Education.FDS);
		System.out.println(lecture2);
		Lecturer lecture3 = new Lecturer(2, 79.0, 170, 28, "Andriy", Gender.MALE, Education.PHD);
		System.out.println(lecture3);
		Lecturer lecture4 = new Lecturer(2, 79.0, 170, 28, "Natalya", Gender.MALE, Education.PHD);
		System.out.println(lecture4);
		
		lecture1.create(); // what about other methods: God can create
		lecture2.talk();
		lecture2.eat();
		lecture2.sleep();
		
		lecture3.create();
		lecture3.eat();
		lecture3.sleep();
		lecture3.talk();
		lecture3.teach(student2);
		lecture3.teach(student3);
		lecture3.teach(dog1);
		lecture3.teach(dog2);
		lecture3.teach(dog3);
		
		lecture3.love(lecture4);
		lecture3.love(dog3);
		dog2.love(dog3);
		
		
		
		

	}

}

package ua.edu.tntu.fis.javaclass.entitites;

public interface Swimmable {
	
	default void swim(int longitude, int latitude) {
		System.out.printf("I'm swimming to %d %d \n", longitude, latitude);
	}
}

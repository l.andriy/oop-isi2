package ua.edu.tntu.fis.javaclass.entitites;

public class Bug {
	private String name;
	
	
	public Bug(String name) {
		super();
		this.name = name;
	}


	public void bite(Animal animal) {
		System.out.println("Hi, I'm a bug " + name + " I'm bitting " + animal.getName());
	}


	@Override
	public String toString() {
		return "Bug [name=" + name + "]";
	}
	
	

}

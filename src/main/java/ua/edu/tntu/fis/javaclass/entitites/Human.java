package ua.edu.tntu.fis.javaclass.entitites;

import ua.edu.tntu.fis.javaclass.entitites.organs.Brain;
import ua.edu.tntu.fis.javaclass.entitites.organs.Heart;

public class Human extends Animal implements God, Swimmable {

	
	
	
	public Human() {
		super();
		// TODO Auto-generated constructor stub
	}

	


	public Human(int id, double weight, double height, int age, String name, Bug[] bugs, Gender sex) {
		super(id, weight, height, age, name, bugs, sex);
		// TODO Auto-generated constructor stub
	}




	public Human(int id, double weight, double height, int age, String name, Gender sex) {
		super(id, weight, height, age, name, sex);
		// TODO Auto-generated constructor stub
	}




	public Human(int id, double weight, double height, int age, Brain brain, Heart heart, Stomach stomach, Bug[] bugs,
			String name, Gender sex) {
		super(id, weight, height, age, brain, heart, stomach, bugs, name, sex);
	}



	@Override
	public Object create() {
		return new String("I'm creating... it is from God");
	}
	
	

	@Override
	public void swim(int longitude, int latitude) {
		System.out.print("I'm a human: ");
		Swimmable.super.swim(longitude, latitude);
	}


	@Override
	public void talk() {
		System.out.println("I'm talking to other humans to share my thoughts and my emotions.");

	}




	@Override
	public void love(Animal animal) {
		if (animal instanceof Human) {
			System.out.println(this.getName() + " loves " + animal.getName());
		} else {
			System.out.println("it is impossible to have love");
		}
		
	}

}

package ua.edu.tntu.fis.javaclass.entitites.organs;

public class Heart {
	
	void pump() {
		System.out.println("I'm a heart and I'm pumping a blood");
	}

	@Override
	public String toString() {
		return "Heart []";
	}
	
	

}

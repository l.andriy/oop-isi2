package ua.edu.tntu.fis.javaclass.entitites;

import java.util.Arrays;

import ua.edu.tntu.fis.javaclass.entitites.organs.Brain;
import ua.edu.tntu.fis.javaclass.entitites.organs.Heart;

public abstract class Animal {
	private int id = 0;
	private double weight;
	private double height;
	private int age;
	private Brain brain;
	private Heart heart;
	private int health;
	private Stomach stomach;
	private Bug[] bugs;
	
	private String name;
	private Gender sex;
	
	// inner class 
	public static class Stomach {
		private static final int NUMBER_OF_FISHES = 10;
		private Food[] food;
		private int foodCount = 0;

		public Stomach() {
			super();
			this.food = new Food[NUMBER_OF_FISHES];
		}
	
		
		public int swallow(Food...food) {
			int health = 0;
			for (Food f: food) {
				if (notFull()) {
					food[foodCount++] = f;
					health += 10;
				};				
			}
			return health;
		}
		
		public boolean notFull() {
			return this.food.length < NUMBER_OF_FISHES;
		}
		
		public boolean isFull() {
		   return this.food.length == NUMBER_OF_FISHES;
		}

	}
	
	//default
	public Animal() {
		super();
	}
	// super class - Object
	
	public Animal(int id, double weight, double height, int age, String name, Bug[] bugs, Gender sex) {
		super(); // calling of the default constructor of the parent class
		this.id = id;
		this.weight = weight;
		this.height = height;
		this.age = age;
		this.brain = new Brain();
		this.heart = new Heart();
		this.stomach = new Stomach();
		this.bugs = new Bug[100];
		this.name = name;
		this.sex = sex;
		System.out.println("creating " + this.getClass().getSimpleName() + " constructor " + name);
	}
	
	public Animal(int id, double weight, double height, int age, String name, Gender sex) {
		super(); // calling of the default constructor of the parent class
		this.id = id;
		this.weight = weight;
		this.height = height;
		this.age = age;
		this.brain = new Brain();
		this.heart = new Heart();
		this.stomach = new Stomach();
		this.bugs = new Bug[100];
		this.name = name;
		this.sex = sex;
		System.out.println("creating " + this.getClass().getSimpleName() + " constructor " + name);
	}

	public Animal(int id, double weight, double height, int age, Brain brain, Heart heart, Stomach stomach, Bug[] bugs,
			String name, Gender sex) {
		super(); // calling of the default constructor of the parent class
		this.id = id;
		this.weight = weight;
		this.height = height;
		this.age = age;
		this.brain = new Brain();
		this.heart = new Heart();
		this.stomach = new Stomach();
		this.bugs = new Bug[100];
		this.name = name;
		this.sex = sex;
		System.out.println("creating " + this.getClass().getSimpleName() + " constructor " + name);
	}
		
	public Animal(int id, double weight, int age, Bug[] bugs, String name, Gender sex) {
		super(); // calling of the default constructor of the parent class
		this.id = id;
		this.weight = weight;
		this.age = age;
		this.brain = new Brain();
		this.heart = new Heart();
		this.stomach = new Stomach();
		this.bugs = new Bug[100];
		this.name = name;
		this.sex = sex;
	}

	public void eat(Food...food) {
		for (Food f : food) {
		  this.health += this.stomach.swallow(food);
		  this.weight += ((Animal) f).getWeight();
		}		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Bug[] getBugs() {
		return bugs;
	}

	public void setBugs(Bug[] bugs) {
		this.bugs = bugs;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getSex() {
		return sex;
	}

	public void setSex(Gender sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "[id=" + id + ", weight=" + weight + ", height=" + height + ", age=" + age + ", brain=" + brain
				+ ", heart=" + heart + ", health=" + health + ", stomach=" + stomach + ", bugs=" + Arrays.toString(bugs)
				+ ", name=" + name + ", sex=" + sex + "]";
	}
	
	public abstract void talk(); 
	
	public void eat() {
		System.out.println(name + " eating now");
	}
	
	public void sleep() {
		System.out.println(name + " sleeping now");
	}
	
	public abstract void love(Animal animal);

	
}

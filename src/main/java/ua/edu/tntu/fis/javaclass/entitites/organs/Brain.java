package ua.edu.tntu.fis.javaclass.entitites.organs;

public class Brain {
	
	void think() {
		System.out.println("I'm a brain and I'm thinking now");
	}

	@Override
	public String toString() {
		return "Brain []";
	}
	

}

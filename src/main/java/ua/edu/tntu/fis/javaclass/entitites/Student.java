package ua.edu.tntu.fis.javaclass.entitites;

import java.util.Arrays;

import ua.edu.tntu.fis.javaclass.entitites.organs.Brain;
import ua.edu.tntu.fis.javaclass.entitites.organs.Heart;

public class Student extends Human {
	
	private double excellence;
	
	public Student() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	public Student(int id, double weight, double height, int age, Brain brain, Heart heart, Stomach stomach, Bug[] bugs,
			String name, Gender sex) {
		super(id, weight, height, age, brain, heart, stomach, bugs, name, sex);
		// TODO Auto-generated constructor stub
	}



	public Student(int id, double weight, double height, int age, String name, Bug[] bugs, Gender sex, double excellence) {
		super(id, weight, height, age, name, bugs, sex);
		// TODO Auto-generated constructor stub
		this.excellence = excellence;
	}



	public Student(int id, double weight, double height, int age, String name, Gender sex, double excellence) {
		super(id, weight, height, age, name, sex);
		// TODO Auto-generated constructor stub
		this.excellence = excellence;
	}



	public Student(int id, double weight, double height, int age, Brain brain, Heart heart, Stomach stomach, Bug[] bugs,
			String name, Gender sex, double excellence) {
		super(id, weight, height, age, brain, heart, stomach, bugs, name, sex);
		// TODO Auto-generated constructor stub
		this.excellence = excellence;
	}

	public void study() {
		System.out.println("I'm studying");
	}

	@Override
	public String toString() {
		return "Student [excellence=" + excellence + ", create()=" + create() + ", getId()=" + getId()
				+ ", getWeight()=" + getWeight() + ", getHeight()=" + getHeight() + ", getAge()=" + getAge()
				+ ", getBugs()=" + Arrays.toString(getBugs()) + ", getName()=" + getName() + ", getSex()=" + getSex()
				+ ", toString()=" + super.toString() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ "]";
	}
	
	

}
